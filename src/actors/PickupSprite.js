import Sprite from './Sprite';

/** Class that is used to create pickup effect. Floats up and dissolves for 2 seconds. */
export default class PickupSprite extends Sprite {
  constructor(spriteIndex) {
    super(spriteIndex);
    this.lastUpdated = Date.now();
  }

  /**
   * Overriden function that also calculates alpha and y position of sprite
   */
  draw() {
    const dTime = Date.now() - this.lastUpdated;
    this.lastUpdated = Date.now();
    this.y -= dTime * 0.05;
    this.alpha -= dTime * 0.0005;
    super.draw();
  }
}
