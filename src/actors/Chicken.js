import Animal from './Animal';

export default class Chicken extends Animal {
  constructor(gameInstance) {
    super(2, 1, 10, 30, gameInstance);
  }
}
