import Cell from './Cell';
import Game from '../Game';

/** Parental class for Cows and Chickens */
export default class Animal extends Cell {
  /**
   * Create an animal
   * @param {Number} spriteIndex - Image index within the spritesheet, zero-based
   * @param {Number} resourceIndex - Resource that animal produces, where 0 is wheat, 1 is egg, 2 is milk
   * @param {Number} resourceInterval - Resource production interval, in seconds
   * @param {Number} foodSaturation - Indicates how long does the food last, in seconds
   * @param {Game} gameInstance - A global Game object
   */
  constructor(
    spriteIndex,
    resourceIndex,
    resourceInterval,
    foodSaturation,
    gameInstance
  ) {
    super(spriteIndex, gameInstance);
    this.resourceIndex = resourceIndex;
    this.resourceInterval = resourceInterval;
    this.foodSaturation = foodSaturation;
    this.feedTime = Date.now();

    this.resourceTimeout = setTimeout(
      () => this.produceResource(),
      this.resourceInterval * 1000
    );
  }

  
  /**
   * Timeout function that produces a resource if not hungry
   */
  produceResource() {
    if (Date.now() - this.feedTime <= this.foodSaturation * 1000) {
      Game.state.resources[this.resourceIndex]++;
      this.addPickupSprite(3 + this.resourceIndex);

      this.resourceTimeout = setTimeout(
        () => this.produceResource(),
        this.resourceInterval * 1000
      );
    }
  }

  /**
   * Overriden function that draws a saturation bar underneath the sprite
   */
  draw() {
    const dTime = Date.now() - this.feedTime;

    super.drawBar(
      '#B44419',
      '#DB501E',
      Math.max(
        (this.foodSaturation * 1000 - dTime) / (this.foodSaturation * 1000),
        0
      )
    );

    super.draw();
  }

  /**
   * Function that feeds the animal on click
   */
  clickAction() {
    if (Game.state.resources[0] > 0) {
      // if been hungry, start resource production again
      if (Date.now() - this.feedTime > this.foodSaturation * 1000) {
        clearTimeout(this.resourceTimeout);

        this.resourceTimeout = setTimeout(
          () => this.produceResource(),
          this.resourceInterval * 1000
        );
      }
      this.feedTime = Date.now();

      // remove wheat
      Game.state.resources[0]--;
    }
  }
}
