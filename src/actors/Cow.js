import Animal from './Animal';

export default class Cow extends Animal {
  constructor(gameInstance) {
    super(1, 2, 20, 20, gameInstance);
  }
}
