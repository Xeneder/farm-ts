import Cell from './Cell';
import Canvas from '../Canvas';
import Game from '../Game';

/** Class that represents wheat */
export default class Wheat extends Cell {
  /**
   * Creates wheat
   * @param {Game} gameInstance - A global Game object 
   */
  constructor(gameInstance) {
    super(6, gameInstance);
    // the time that it takes the wheat to fully grow, in seconds
    this.growthTimeout = 10;
    // growth start time
    this.plantTime = Date.now();
  }

  /**
   * Overriden function that draws a growth bar underneath the sprite
   */
  draw() {
    const dTime = Date.now() - this.plantTime;
    // update the animation up to a 4th phase
    this.phase = Math.min(
      Math.floor((dTime / (this.growthTimeout * 1000)) * 4),
      4
    );

    super.drawBar(
      '#32B218',
      '#45E820',
      Math.min(dTime / (this.growthTimeout * 1000), 1)
    );

    super.draw();
  }

  /**
   * Function that harvests the wheat
   */
  clickAction() {
    if (this.phase < 4) {
      return;
    }

    this.plantTime = Date.now();

    this.addPickupSprite(3);

    // add wheat
    Game.state.resources[0]++;
  }
}
