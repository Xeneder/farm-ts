import Canvas from '../Canvas';
import Game from '../Game';
import { spritesheet } from '../spritesheet';

/** Class that represent in-game graphical object */
export default class Sprite {
  /**
   * 
   * @param {Number} spriteIndex - Image index within the spritesheet, zero-based
   * @param {Number} gameInstance - A global Game object
   */
  constructor(spriteIndex, gameInstance) {
    this.spriteIndex = spriteIndex;
    this.gameInstance = gameInstance;
    this.x = 0;
    this.y = 0;
    this.height = 0;
    this.width = 0;
    this.alpha = 1;
  }

  /**
   * Draws the sprite
   */
  draw() {
    Canvas.context.globalAlpha = Math.max(0, this.alpha);
    Canvas.context.drawImage(
      spritesheet,
      256 * (this.spriteIndex + (this.phase || 0)),
      0,
      256,
      256,
      this.x,
      this.y,
      this.width,
      this.height
    );
    Canvas.context.globalAlpha = 1;
  }

  /**
   * Draws progress bar at top of the sprite
   * @param {String} primaryColor - Back color of the bar
   * @param {String} secondaryColor - Highlight color of the bar
   * @param {Number} percent - [0, 1] float that is used to determine the bar width
   */
  drawBar(primaryColor, secondaryColor, percent) {
    Game.drawRect(
      this.x,
      this.y,
      Math.min(Canvas.height / 8, (Canvas.height / 8) * percent),
      Canvas.height / 128,
      primaryColor,
      0.8
    );

    Game.drawRect(
      this.x,
      this.y + Canvas.height / 512,
      (Canvas.height / 8) * percent,
      Canvas.height / 256,
      secondaryColor,
      0.8
    );
  }

  /**
   * A function that handles the click and executes the action if needed
   * @param {MouseEvent} event - mouse click event
   * @param {Boolean} executeAction - If true, executes the clickAction() function
   * @return {Boolean} Indicates whether the mouse click landed on the sprite or not
   */
  clickHandler(event, executeAction = true) {
    if (
      event.offsetX >= this.x &&
      event.offsetY >= this.y &&
      event.offsetX <= this.x + this.width &&
      event.offsetY <= this.y + this.height
    ) {
      if (executeAction) {
        this.clickAction();
      }
      return true;
    }
    return false;
  }

  /**
   * Placeholder function that is used when sprited is clicked on
   */
  clickAction() {}
}
