import Sprite from './Sprite';
import PickupSprite from './PickupSprite';
import Game from '../Game';

/** Class that represent a grid cell. Can be either a Wheat or an Animal */
export default class Cell extends Sprite {
  /**
   * Create a cell
   * @param {Number} spriteIndex - Image index within the spritesheet, zero-based
   * @param {Number} resourceIndex - Resource that animal produces, where 0 is wheat, 1 is egg, 2 is milk
   */
  constructor(spriteIndex, gameInstance) {
    super(spriteIndex || -1);
    this.gameInstance = gameInstance;
  }

  /**
   * Function that spawns a pickup sprite that floats for 2 seconds
   * @param {Number} spriteIndex - Image index within the spritesheet, zero-based
   */
  addPickupSprite(spriteIndex) {
    const pickupSprite = new PickupSprite(spriteIndex);
    pickupSprite.x = this.x;
    pickupSprite.y = this.y;
    pickupSprite.width = this.width;
    pickupSprite.height = this.height;

    this.gameInstance.sprites.push(pickupSprite);
  }
}
