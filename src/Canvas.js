/** Class that handles the canvas logic  */
export default class Canvas {
  constructor() {
    this.canvas = document.getElementById('game-canvas');

    document.body.onresize = this.resizeCanvas.bind(this);
    this.resizeCanvas();
  }

  /**
   * Function that resizes canvas on window resize
   */
  resizeCanvas() {
    this.canvas.width = document.body.offsetWidth;
    this.canvas.height = document.body.offsetHeight;
  }

  static get canvas() {
    return document.getElementById('game-canvas');
  }

  static get width() {
    return Canvas.canvas.width;
  }

  static get height() {
    return Canvas.canvas.height;
  }
}

// static props
Canvas.context = document.getElementById('game-canvas').getContext('2d');
