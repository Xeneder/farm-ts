import { spritesheet } from './spritesheet';
import Sprite from './actors/Sprite';
import Canvas from './Canvas';
import Cell from './actors/Cell';
import Cow from './actors/Cow';
import Chicken from './actors/Chicken';
import Wheat from './actors/Wheat';

/** Main game class */
export default class Game {
  constructor() {
    // 8x8 grid with main objects
    this.grid = [];
    // other sprites floating around
    this.sprites = [];

    for (let i = 0; i < 8; i++) {
      this.grid.push(new Array(8));
      for (let j = 0; j < 8; j++) {
        this.grid[i][j] = new Cell();
      }
    }

    // initial resources setup
    this.grid[1][1] = new Cow(this);
    this.grid[5][4] = new Chicken(this);
    this.grid[6][6] = new Wheat(this);

    spritesheet.onload = () => this.draw();
    spritesheet.src = './img/sprite.png';

    document.onclick = event => this.clickHandler(event);

    this.initInterfaceSprites();
  }

  /**
   * Main draw loop
   */
  draw() {
    Canvas.context.clearRect(0, 0, Canvas.width, Canvas.height);

    const w = Canvas.width;
    const h = Canvas.height;

    // drawing the grid
    for (let i = 0; i < this.grid.length; i++) {
      for (let j = 0; j < this.grid[0].length; j++) {
        this.grid[i][j].x = w - h + (j * h) / 8;
        this.grid[i][j].y = (i * h) / 8;
        this.grid[i][j].height = this.grid[i][j].width = h / 8;

        this.drawBackgroundTile(
          this.grid[i][j].x,
          this.grid[i][j].y,
          (i * 7 + j + 1) % 2
        );
        if (this.grid[i][j].spriteIndex > 0) {
          this.grid[i][j].draw();
        }
      }
    }

    // drawing other sprites
    this.sprites.forEach((sprite, index) => {
      if (sprite.alpha <= 0) {
        this.sprites.splice(index, 1);
      }
      sprite.draw();
    });

    this.drawText();

    requestAnimationFrame(() => this.draw());
  }

  /**
   * Function for drawing background pattern
   * @param {Number} x - X on-screen coordinate to draw tile on
   * @param {Number} y - Y on-screen coordinate to draw tile on
   * @param {Number} isDark - Should be 1 or 0. If 1, the tile is slightly darkened (used to make a checkerboard pattern)
   */
  drawBackgroundTile(x, y, isDark) {
    Canvas.context.globalAlpha = 1;
    Canvas.context.drawImage(
      spritesheet,
      0,
      0,
      256,
      256,
      x,
      y,
      Canvas.height / 8,
      Canvas.height / 8
    );

    Game.drawRect(x, y, Canvas.height / 8, Canvas.height / 8, '#000000', 0.15 * isDark);
  }

  /**
   * Function that draws all in-game text
   */
  drawText() {
    // font setup
    Canvas.context.font = '20px sans-serif';
    Canvas.context.lineWidth = 4;
    Canvas.context.globalAlpha = 1;

    // drawing money
    Game.drawLabel(`$${Game.state.money}`, 16, 24);

    // drawing resources - wheat, egg, milk
    for (let i = 0; i < Game.state.resources.length; i++) {
      Game.drawLabel(Game.state.resources[i], 68, 64 + i * 64);
    }

    // drawing command pallete
    [
      'Get a cow:  [-$30]',
      'Get a chicken:  [-$10]',
      'Plant wheat:  [-$5]',
      `Sell milk and eggs:  [+$${Game.state.resources[1] * 2 +
        Game.state.resources[2] * 5}]`
    ].forEach((text, index) => {
      Game.drawLabel(text, 16, 256 + index * 64);
    });
  }

  /**
   * Function that draws a text label
   * @param {String} text - Label display text
   * @param {Number} x - Label x on-screen coordinate
   * @param {Number} y - Label y on-screen coordinate
   */
  static drawLabel(text, x, y) {
    Canvas.context.fillStyle = '#000000';
    Canvas.context.strokeText(text, x, y);
    Canvas.context.fillStyle = '#ffffff';
    Canvas.context.fillText(text, x, y);
  }

  /**
   * Function that draws a rectangle
   * @param {Number} x - Rectangle leftmost on-screen coordinate
   * @param {Number} y - Rectangle topmost on-screen coordinate
   * @param {Number} width - Rectangle width
   * @param {Number} height - Rectangle height
   * @param {String} color - Color of the rectangle
   * @param {Number} alpha - Float witin [0, 1] range. 0 is transparent, 1 is opaque
   */
  static drawRect(x, y, width, height, color, alpha) {
    Canvas.context.globalAlpha = alpha;
    Canvas.context.fillStyle = color;

    Canvas.context.beginPath();
    Canvas.context.rect(x, y, width, height);
    Canvas.context.fill();
  }

  /**
   * Function that handles clicking inside canvas
   * @param {MouseEvent} event - mouse click event
   */
  clickHandler(event) {
    // if in placement mode
    if (Game.state.objectToPlace !== null) {
      for (let i = 0; i < this.grid.length; i++) {
        for (let j = 0; j < this.grid[0].length; j++) {
          // checking every cell for a click
          if (this.grid[i][j].clickHandler(event, false)) {
            // replace clicked cell with a new one
            this.grid[i][j] = new (Game.state.objectToPlace)(this);
            // turn off the placement mode
            Canvas.canvas.style = '';
            Game.state.objectToPlace = null;
          }
        }
      }
    // if not in placement mode
    } else {
      this.grid.forEach(row => {
        row.forEach(cell => {
          // pass down to individual cell
          cell.clickHandler(event);
        });
      });

      // check if clicked on any of the commands
      [
        () => this.cellPlace(0),
        () => this.cellPlace(1),
        () => this.cellPlace(2),
        () => this.sellEverything()
      ].forEach((f, i) => {
        if (
          event.offsetX > 16 &&
          event.offsetY > 242 + i * 64 &&
          event.offsetX < 240 &&
          event.offsetY < 270 + i * 64
        ) {
          f();
        }
      });
    }
  }

  /**
   * Function to initialize cell object placement
   * @param {Number} cellIndex - index of object to place. 0 = Cow, 1 = Chicken, 2 = Wheat
   */
  cellPlace(cellIndex) {
    // check if have the money
    const moneyRequired = [30, 10, 5];
    if (Game.state.money >= moneyRequired[cellIndex]) {
      const cells = [Cow, Chicken, Wheat];
      // initialize object placement mode
      Game.state.objectToPlace = cells[cellIndex];
      // set the cursor to crosshair
      Canvas.canvas.style = 'cursor: crosshair';
    }
  }

  /**
   * Sells milk and eggs. Every milk is worth $5, every egg - $2
   */
  sellEverything() {
    Game.state.money +=
      Game.state.resources[1] * 2 + Game.state.resources[2] * 5;
    Game.state.resources[1] = Game.state.resources[2] = 0;
  }

  /**
   * Function that initalizes interface sprites that are used to indicate resources
   */
  initInterfaceSprites() {
    for (let i = 0; i < 3; i++) {
      const interfaceSprite = new Sprite();
      interfaceSprite.x = 0;
      interfaceSprite.y = 24 + i * 64;
      interfaceSprite.width = interfaceSprite.height = 64;
      interfaceSprite.spriteIndex = 3 + i;

      this.sprites.push(interfaceSprite);
    }
  }
}

// static props

Game.state = {
  // wheat, eggs, milk
  resources: [0, 0, 0],
  money: 0,
  objectToPlace: null
};
